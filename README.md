# Splash Page

Native Flutter Splash Page

pub.dev - [flutter_native_splash](https://pub.dev/packages/flutter_native_splash)

## Dependency

Add flutter_native_splash as a dev dependency in your pubspec.yaml file.

_pubspec.yaml_

```yaml
dev_dependencies:
  flutter_native_splash: ^1.2.1
```

```console
flutter pub get
```

## Configuration

Customize settings in a file in your root project folder.

_flutter_native_splash.yaml_

```yaml
flutter_native_splash:
  color: "#42a5f5"
  image: assets/splash.png
  android: false
  ios: false
  web: false
  android12: true
```

## Run the package

After adding your settings, run the following command in the terminal.

```console
flutter pub run flutter_native_splash:create --path=flutter_native_splash.yaml
```

## Result

<p>
<img src="https://gitlab.com/pascal.cantaluppi/splashscreen/-/raw/main/assets/preview.png" alt="Preview" />
</p>
